# MIDI Auto Connect

Start and keep a fluidsynth process running. Auto-Connect any MIDI-device to it.

This uses udev to detect newly plugged in devices, and it runs fluidsynth as a systemd service


## Requirements/Setup

MIDI Auto Connect makes use of these programs and libraries.

- systemd
- udev
- alsa
- fluidsynth
- ruby
- git (optional, but easier to keep up to date)

On a debian/ubuntu style system (for example raspbian/Raspberry Pi OS including Lite/Minimal) run these commands:

- `sudo apt update` update your package manager's information
- `sudo apt upgrade` is not *strictly* neccessary, but doesn't hurt to start with up to date versions
- `sudo apt install alsa fluidsynth ruby git` should install the dependencies
- `git clone https://codeberg.org/ccoenen/MIDI-Auto-Connect.git` to get the files
- `cd MIDI-Auto-Connect`
- `./setup.sh`


## Usage

Plug any USB-MIDI device into any of the USB-Ports and it should be picked up automatically. This will also autostart after any reboot.


## Updating and Tinkering

Any change to `find-and-connect.rb` will automatically apply the next time you plug or unplug a MIDI device. For changes to the udev-rules or systemd-service, it is easiest to just run `./setup.rb`.

So, when in doubt, just run `./setup.rb`.


## Uninstall

remove the symlink in /etc/udev
disable the service with `systemctl --user disable fluidsynth`


## Notes

Currently this is written in a way that should work immediately on any raspberry pi. If you wish to run this in a different environment, you will probably need to correct some paths. If you want to work on making this more generalized, I would welcome a pull request.


## Useful, related commands

- `alsamixer` set your volume levels
- `aplaymidi -p 128:0 <midi-file-name.mid>` play a midi file to an output
- `aconnect -l` list all midi devices


## References and other helpful links

These links helped me along the way. In no particular order. Well, perhaps in somewhat chronological order.
- https://neuma.studio/rpi-midi-complete.html
- https://www.youtube.com/watch?v=LMDs44wQaK0
- https://bbs.archlinux.org/viewtopic.php?id=135092
- https://wiki.archlinux.org/index.php/FluidSynth#ALSA_daemon_mode
- https://wiki.archlinux.org/index.php/Systemd/User#Automatic_start-up_of_systemd_user_instances
- http://andrewdotni.ch/blog/2015/02/28/midi-synth-with-raspberry-p/
- https://github.com/systemd/systemd/issues/2690#issuecomment-186973730
