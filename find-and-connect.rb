#!/usr/bin/env ruby

def aconnect_devices(aconnect_output)
  ### EXAMPLE OUTPUT
  # client 0: 'System' [type=Kernel]
  #     0 'Timer           '
  #     1 'Announce        '
  # client 14: 'Midi Through' [type=Kernel]
  #     0 'Midi Through Port-0'
  # client 20: 'LPD8' [type=Kernel,card=1]
  #     0 'LPD8 MIDI 1     '

  devices = []

  aconnect_output.split('client').each do |chunk|
    current_device = nil

    chunk.lines.each do |line|
      /^\s+(\d*):\s+'([^']+)'\s+(.+)$/ =~ line
      if $1 && $1 != '0'
        device = $1
        name = $2
        info = $3

        # we skip the "Through" port
        if /^Midi Through$/ =~ name
          puts "skipping #{name}, device #{device}" if $DEBUG
          current_device = nil
          break
        end

        puts "found device: '#{name}' on ID #{device}."
        current_device = {name: name, device: device, info: info, ports: []}
        devices << current_device
      elsif current_device
        /^\s+(\d+)\s+'([^']+)'\s*$/ =~ line
        port = $1
        name = $2
        if port && name
          puts "  found port #{port} / #{name}"
          current_device[:ports] << port
        else
          puts "not recognized: #{line}" if $DEBUG
        end
      end
    end
  end

  devices
end

def connect(input, output)
  puts "trying to connect #{input} to #{output}"
  input[:ports].each do |input_port|
    output[:ports].each do |output_port|
      command = "aconnect #{input[:device]}:#{input_port} #{output[:device]}:#{output_port}"
      puts command if $DEBUG
      system command
    end
  end
end

input_devices = aconnect_devices(`aconnect -l -i`)
output_devices = aconnect_devices(`aconnect -l -o`)

if $DEBUG
  puts "inputs:"
  puts input_devices
  puts "\noutputs:"
  puts output_devices
end

output_device = output_devices.keep_if{|d| d[:name].match?(/fluidsynth-service/i) }.first
if output_device
  input_devices.each do |input_device|
    connect(input_device,output_device)
  end
else
  puts "no output device found. Is Fluidsynth running?"
  exit(1)
end
