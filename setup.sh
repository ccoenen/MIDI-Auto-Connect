#!/bin/sh
sudo ln -s `realpath ./midiusb.rules` /etc/udev/rules.d/33-midiusb.rules

sudo udevadm control --reload
sudo service udev restart


mkdir -p ~/.config/systemd/user/
cp ./fluidsynth.service ~/.config/systemd/user/

sudo systemctl daemon-reload
systemctl --user enable fluidsynth.service
systemctl --user start fluidsynth.service

# this makes sure the user's service files get also run at system boot
loginctl enable-linger pi

